# Changelog

## 0.8.0
Heavy refactoring:
- removed query methods: use iterators or Vec's drain()/retain() methods instead
- removed C feature. Use the pure C airone implementation instead.
- a lot of cleaning and code reorganization
- removed the need for the airone_init! macro, now the derive macro is enough
- added choice of buffering mode
- added choice of savemode (instantaneous or manually saved/rollback'd)
- reorganized serialization code
- external crates can now implement airone's serialize traits to add support for new types
- forbidden unsafe code and everything is implemented in safe Rust


## 0.7.1
Added example folder.

Method `bulk_push` added again. Now it takes an iterator as an argument.

## 0.7.0
Airone now has an optional `c` feature that generates a pure C header file: in this way, you can use airone in C programs as an external library. See <https://gitlab.com/MassiminoilTrace/airone_c_template> for more information.

## 0.6.0
Completely replaced the `airone_db!` macro for a procedural macro called `AironeDbDerive`. This solves the issue encountered with Rocket framework integration, particularly for its `FromForm` trait.

The `set` method in `QueryMut` has been removed.

## 0.5.1
Small documentation change

## 0.5.0
Breaking change:
- the data format for the dump file also has column names in the first line as a header
- order and naming consistency between the columns in the file and struct fields is enforced at startup

## 0.4.2
- Internal refactor to reduce code size

## 0.4.2
Separated Query and QueryMut so the first one only has read-only operations

## 0.4.0
- Big rewrite
- added transparent get_mut(index) method
- removed external access to the internal database methods
- no custom error type. Accessing an invalid index just panics

## 0.3.3
- Shrinking the memory occupation of the vector after the compaction startup procedure

## 0.3.2
- Detecting capacity at startup to reduce the number reallocations, leveraing the `Vec::with_capacity` method

## 0.3.1
- Removed variable_type from edit operation file format. It was reintroduced by mistake during a merge. Make sure to compact existing data as the new transaction log might not be compatible with the old one.

## 0.3.0
- Refactored to use generic type for inner element
- renamed main object to always use the name AironeDb

## 0.2.6
- Fixed bug when defining multiple Airone objects
- Added `prelude` file for convenience
